<?php
require __DIR__ . '/vendor/autoload.php';
$credentials = new \Del\Common\Config\DbCredentials();
$containerSvc = \Del\Common\ContainerService::getInstance();

$depend = require_once('migrant.php');
$db = $depend['db'];

$credentials->setDriver($db['pdo_pgsql'])
    ->setDatabase($db['dbadmin'])
    ->setUser($db['dbadmin'])
    ->setHost($db['127.0.0.1'])
    ->setPassword($db['dbadmin']);

$containerSvc->setDbCredentials($credentials);

$userPackage = new \Del\UserPackage();
$containerSvc->registerToContainer($userPackage);

$container = $containerSvc->getContainer(); //ready to rock