<?php

use Del\Common\Config\DbCredentials;
use Del\Common\ContainerService;
use Del\UserPackage;

$userPackage = new UserPackage();

$credentials = new DbCredentials([
    'driver' => 'pdo_pgsql',
    'dbname' => 'mydb',
    'user' => 'dbadmin',
    'password' => 'dbadmin',
]);


$containerSvc = ContainerService::getInstance();
$containerSvc->setDbCredentials($credentials);
$containerSvc->registerToContainer($userPackage);